<?php

namespace App;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class FormatPrice extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('package_price', [$this, 'formatPackagePrice']),
        ];
    }

    public function formatPackagePrice($number)
    {
        $price = number_format($number, 0, ',', '.');
        $price = 'Rp ' . $price . '/';

        return $price;
    }
}
