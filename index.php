<?php
require 'vendor/autoload.php';

use App\FormatPrice;

$loader = new \Twig\Loader\FilesystemLoader('templates');
$twig = new \Twig\Environment($loader, [
    'debug' => true,
    'cache' => false,
    'auto_reload' => true
]);

$priceFilter = new \Twig\TwigFilter('package_price', [new FormatPrice(), 'formatPackagePrice']);
$twig->addFilter($priceFilter);

$jsonPackages = file_get_contents('data/packages.json');
$contentPackages = json_decode($jsonPackages);

$jsonPackages = file_get_contents('data/php_modules.json');
$contentModules = json_decode($jsonPackages);

echo $twig->render('index.twig', [
    'contentPackages' => $contentPackages,
    'contentModules' => $contentModules
]);