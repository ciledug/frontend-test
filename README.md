## About 

This is a simple frontend website built on native [PHP](https://www.php.net), [Twig](https://twig.symfony.com) and [Bootstrap](https://getbootstrap.com) stack as a job test for Fullstack Developer.

## Requirements

Composer have to be installed to download some packages and dependencies needed to run this frontend.

## Framework

- PHP 8.0.2 (XAMPP 8.0.2) - [https://www.apachefriends.org](https://www.apachefriends.org)
- Twig 3.3 - [https://twig.symfony.com](https://twig.symfony.com)
- Bootstrap 5.0.2 - [https://getbootstrap.com](https://getbootstrap.com)

## Docker

To build the Dockerfile, please follow the instructions below by typing them inside the command/console window:

- docker build -t togie/landing-page:latest .
- docker container create -p 80:80 --name landing-page togie/landing-page:latest
- docker container start landing-page
- docker exec -ti landing-page sh -c "php-fpm7"

Please note 'togie/landing-page:latest' and 'landing-page' are custom names. One should change them to any name you want.

## Disclaimer

DO NOT USE THIS APPLICATION FOR PRODUCTION USED!