FROM nginx:stable-alpine

RUN apk update && apk upgrade
RUN apk add nano
RUN mkdir /www/

# installing php-fpm7
RUN apk add php7-fpm php7-mcrypt php7-soap php7-openssl php7-gmp php7-pdo_odbc php7-json php7-dom php7-pdo php7-zip php7-mysqli php7-sqlite3 php7-apcu php7-pdo_pgsql php7-bcmath php7-gd php7-odbc php7-pdo_mysql php7-pdo_sqlite php7-gettext php7-xmlreader php7-xmlrpc php7-bz2 php7-iconv php7-pdo_dblib php7-curl php7-ctype
ENV PHP_FPM_USER="www"
ENV ENV PHP_FPM_GROUP="www"
ENV PHP_FPM_LISTEN_MODE="0660"
ENV PHP_MEMORY_LIMIT="512M"
ENV PHP_MAX_UPLOAD="50M"
ENV PHP_MAX_FILE_UPLOAD="200"
ENV PHP_MAX_POST="100M"
ENV PHP_DISPLAY_ERRORS="ON"
ENV PHP_DISPLAY_STARTUP_ERRORS="ON"
ENV PHP_ERROR_REPORTING="E_COMPILE_ERROR\|E_RECOVERABLE_ERROR\|E_ERROR\|E_CORE_ERROR"
ENV PHP_CGI_FIX_PATHINFO=0

# modified configuration file(s) for nginx
COPY docker_files/fastcgi_params /etc/nginx/
RUN chmod 0644 /etc/nginx/fastcgi_params
COPY docker_files/fastcgi.conf /etc/nginx/
RUN chmod 0644 /etc/nginx/fastcgi.conf
COPY docker_files/default.conf /etc/nginx/conf.d/
RUN chmod 0644 /etc/nginx/conf.d/default.conf

# modified configuration file(s) for php
COPY docker_files/php-fpm.conf /etc/php7/
RUN chmod 0644 /etc/php7/php-fpm.conf
COPY docker_files/www.conf /etc/php7/php-fpm.d/
RUN chmod 0644 /etc/php7/php-fpm.d/www.conf

COPY . /www/

EXPOSE 80

RUN php-fpm7